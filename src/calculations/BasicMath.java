package calculations;
import errors.MathError;

public class BasicMath {

	public double abs(double a) {
		if (a < 0)
			a = a*-1.0;
		
		return a;
	}

	public double add(double a, double b) {
		return a + b;
	}
	
	public double sub(double a, double b) {
		return a - b;
		
	}
	
	public double div(double a, double b) {
		if(b == 0) {
			MathError.divByZero();
		}
		
		return a/b;
		
	}
	
	public double mul(double a, double b) {
		return a*b;
	}
	
	public int mod(int a, int b) {
		return a%b;
	}
	
	
}
