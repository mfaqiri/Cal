/*@author Mansoor Faqiri
 * 
 */

package parser;
import errors.InputError;

/*
 * 
 */
public class OperationParser {
	String input;
	
	private class number {
		//end of the substring inside the main string that holds the number.
		int end = -1;
		//beginning of the substring inside the main string that holds the number.
		int beg;
		//value of the number.
		int val;
		//number of significant figures the number has.
		int sigfig = 0;
		String num;
		public number(int beg) {
			this.beg = beg;
		}
		
		//Determines the end of the
		public void setEnd(int end) {
			this.end = end;
			parseVal();
		}
		
		public void parseVal() {
			num = input.substring(beg, end);
			int decCheck = 0;

			for(int i = 0; i < num.length(); i++) {
				if(num.charAt(i) == '.')
					decCheck++;
				else
					sigfig++;
				
				if(decCheck > 1)
					InputError.numError();
				if( ((int)num.charAt(i) > 57 || (int)num.charAt(i) < 48) 
						&& (int)num.charAt(i) != 46)
					InputError.numError();
			}
			
			if(decCheck > 0)
				sigfig = num.length();
		}
	}
	
	@SuppressWarnings("unused")
	private class operation {
		
		public operation(char op, String subOp ,number a) {
			
		}
	}
	
	public OperationParser(String input) {
		this.input = input;
	}
	
}
